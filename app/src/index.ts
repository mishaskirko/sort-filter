import * as http from 'http';
import {DataFilterRunner} from "./entities/concrete/DataFilterRunner";

const server = http.createServer((request, response) => {
  if (request.url === '/') {
    const runner = new DataFilterRunner();
    //show result
    const result = JSON.stringify(
      runner.run()
    );
    response.end(result);
  }
});

server.listen(3000, () => {
  console.log('Server listening on port 3000');
});
