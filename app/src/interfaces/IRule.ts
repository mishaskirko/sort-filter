export interface IRule {
  condition: any;
  apply(data: any[]): any[];
}