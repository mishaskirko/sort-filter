import {IncludeRule} from "./IncludeRule";
import {SortRule} from "./SortRule";
import {RuleSet} from "./RuleSet";
import {DataFilter} from "./DataFilter";

export class DataFilterRunner {
  public run(): any[] {
// Читаем входные данные
    const input = '{"data": [{"name": "John", "email": "john2@mail.com"}, {"name": "John", "email": "john1@mail.com"}, {"name": "Jane", "email": "jane@mail.com"}], "condition": {"include": [{"name": "John"}], "sort_by": ["email"]}}';
    const {data, condition} = JSON.parse(input);
    // Создаем объекты правил
    const includeRule = new IncludeRule(condition.include[0]);
    const sortRule = new SortRule(condition.sort_by);

// Создаем объект RuleSet и добавляем в него правила
    const ruleSet = new RuleSet();
    ruleSet.addRule(includeRule);
    ruleSet.addRule(sortRule);

    // Создаем объект DataFilter и применяем правила к данным
    const dataFilter = new DataFilter();
// Выводим результат
    return dataFilter.filterData(data, ruleSet);
  }
}