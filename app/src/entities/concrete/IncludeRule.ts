import {Rule} from "../abstractions/Rule";

 export class IncludeRule extends Rule {
  public apply(data: any[]): any[] {
    // Отбираем только те объекты, которые удовлетворяют условию
    return data.filter((item) => {
      for (const key in this.condition) {
        if (item[key] !== this.condition[key]) {
          return false;
        }
      }
      return true;
    });
  }
}