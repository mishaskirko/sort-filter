// Класс DataFilter реализует функционал фильтрации и сортировки данных
import {RuleSet} from "./RuleSet";

export class DataFilter {
  public filterData(data: any[], ruleSet: RuleSet): any[] {
    let result = data;
// Применяем все правила в RuleSet к данным
    for (const rule of ruleSet.rules) {
      result = rule.apply(result);
    }
    return result;
  }
}