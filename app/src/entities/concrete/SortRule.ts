import {Rule} from "../abstractions/Rule";

// Класс SortRule реализует правило сортировки объектов по ключам
export class SortRule extends Rule {
  public apply(data: any[]): any[] {
    // Сортируем
    return data.sort((a, b) => {
      for (const key of this.condition) {
        if (a[key] > b[key]) {
          return 1;
        } else if (a[key] < b[key]) {
          return -1;
        }
      }
      return 0;
    });
  }
}