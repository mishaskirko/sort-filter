// Класс RuleSet хранит список правил и предоставляет методы для их добавления и удаления
import {Rule} from "../abstractions/Rule";

export class RuleSet {
  public rules: Rule[] = [];

  public addRule(rule: Rule): void {
    this.rules.push(rule);
  }

  public removeRule(rule: Rule): void {
    this.rules = this.rules.filter((item) => item !== rule);
  }
}

