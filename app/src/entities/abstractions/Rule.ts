import {IRule} from "../../interfaces/IRule";

export abstract class Rule implements IRule {
  // Свойство condition будет хранить условие для применения правила
  public condition: any;

  constructor(condition: any) {
    this.condition = condition;
  }

  // Метод apply применяет правило к данным и возвращает результат
  public apply(data: any[]): any[] {
    // Реализация метода apply будет определена в наследниках
    return data;
  }
}