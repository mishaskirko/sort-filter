include .env

build: # build all dockerfiles, if not built yet
	@docker-compose -f ${DOCKER_COMPOSE_FILE} build

up: # create and start containers
	@docker-compose -f ${DOCKER_COMPOSE_FILE} up -d

down: # stop and destroy containers
	@docker-compose -f ${DOCKER_COMPOSE_FILE} down

down-volume: # stop and destroy containers and volumes
	@docker-compose -f ${DOCKER_COMPOSE_FILE} down -v

ps: # show started containers and their status
	@docker-compose -f ${DOCKER_COMPOSE_FILE} ps

logs: # show logs of all containers
	@docker-compose -f ${DOCKER_COMPOSE_FILE} logs -f

connect-app: # connect to app container
	@docker-compose -f ${DOCKER_COMPOSE_FILE} exec app sh

npm-run-build: # run npm run build
	@docker-compose -f ${DOCKER_COMPOSE_FILE} exec app npm run build

npm-run-start: # run npm run start
	@docker-compose -f ${DOCKER_COMPOSE_FILE} exec app npm run start

npm-run-dev: # run npm run dev
	@docker-compose -f ${DOCKER_COMPOSE_FILE} exec app npm run dev

npm-install: # run npm install
	@docker-compose -f ${DOCKER_COMPOSE_FILE} exec app npm install

npm-update: # run npm update
	@docker-compose -f ${DOCKER_COMPOSE_FILE} exec app npm update